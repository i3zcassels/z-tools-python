__author__ = 'zack.cassels'

import Tests.PureCloudTests, sys

import logging, time
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

from Tests.PureCloudTests import PureCloudTests
from Common.HTTPResponseLogger import LogWrapper

class Retry:
    @staticmethod
    def until_true(action, timeout, interval):
        timeout = time.time() + timeout
        while time.time() < timeout:
            result = action()
            if result:
                return
            time.sleep(interval)

class ContentManagementBaseTest(PureCloudTests):
    def setUp(self):
        super().setUp()
        self.testUser = self.test_client.get_session()

    def tearDown(self):
        super().tearDown()

    def test_get_org(self):
        with LogWrapper(logger, "TEST " +  sys._getframe().f_code.co_name):
            self.testUser.public_api.get("/api/v1/configuration/organization")
            pass

    def test_get_session(self):
        logger.info("Admin Session -> \"{}\"".format(self.test_client.admin.session_id))
        logger.info("TestUser Session -> \"{}\"".format(self.testUser.session_id))
        pass

    def test_get_edgeGroups(self):
        query = {
            "name": "AutoDcaEdge"
        }
        self.test_client.admin.public_api.get("/api/v1/configuration/edges/", params=query)

    def test_get_self(self):
        with LogWrapper(logger, "TEST " +  sys._getframe().f_code.co_name):
            # self.testUser.public_api.get("/api/v1/contentmanagement/documents?workspaceId={}&name={}".format(self.testUser.))
            pass

    def test_donut_service(self):
        with LogWrapper(logger, "TEST " +  sys._getframe().f_code.co_name):

            user_id = self.testUser.user_id


            # user_id = "93fdd58d-19dc-4ee2-99ac-25baeeeb4cfc"
            # user_id = "ac73b8d6-8cc9-492c-b5f6-7347ea1fd12c"
            #

            # self.testUser.donut.get("/organizations/{}/roles".format(self.org, user_id))
            # self.testUser.donut.get("/organizations/{}/users/{}/roles".format(self.org, user_id))
            # self.testUser.public_api.get("/api/v1/authorization/users/{}/roles".format(user_id))

            # self.testUser.donut.get("/organizations/{}/users/{}/roles".format(self.org, user_id))
            # self.testUser.public_api.get("/api/v1/authorization/users/{}/roles".format(user_id))
            # self.testUser.public_api.get("/api/v1/authorization/users/{}/permissions".format(user_id))
            # self.testUser.donut.get("/organizations/{}/users/{}/roles".format(self.org, user_id))

            # self.testUser.donut.get("/organizations/{}/users/{}".format(self.org, user_id))
            # self.testUser.donut.get("/v1/organizations/{}/users/{}".format(self.org, user_id))
            # self.testUser.donut.get("/v1/organizations/{}/users/{}/policies".format(self.org, user_id))

            time.sleep(10)

            self.testUser.donut.get("/organizations/{}/users/{}".format(self.org, user_id))
            self.testUser.donut.get("/organizations/{}/users/{}/roles".format(self.org, user_id))
            self.testUser.donut.get("/organizations/{}/users/{}/permissions".format(self.org, user_id))
            # self.testUser.public_api.get("/api/v1/authorization/users/{}/roles".format(user_id))
