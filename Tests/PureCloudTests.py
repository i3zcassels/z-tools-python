__author__ = 'zack.cassels'

import unittest, logging.config, logging, uuid, yaml, time
logging.config.fileConfig('logging.ini')
logging.getLogger("requests").setLevel(logging.WARNING)
from Common.HTTPResponseLogger import verboseLog
from TestClient import Test_Client, Environment
logger = logging.getLogger(__name__)

class Retry:
    @staticmethod
    def until_true(action, timeout, interval):
        timeout = time.time() + timeout
        while time.time() < timeout:
            result = action()
            if result:
                return
            time.sleep(interval)


class PureCloudTests(unittest.TestCase):
    def setUp(self):
        env = Environment.DEVELOPMENT
        self.use_cache = True
        self.test_client = Test_Client(env)
        self.org = self.test_client.config.organization

    def tearDown(self):
        self.test_client.close()

    def test_get_session(self):
        self.test_client.admin.public_api.get("/api/v1/sessions/{}".format(self.test_client.admin.session_id))

    def test_feature_flag(self):
        self.test_client.admin.public_api.get("/api/v1/featuretoggles")

    class JavaJSONPrinter():
        def print_java(self, map):
            self.isPrintingList = False
            self.tabs = 0

            def print_level(map):
                for prop in map:
                    if isinstance(map[prop], str):
                        print("{}public String {};".format(self.tabs*"\t", prop))
                        continue
                    if isinstance(map[prop], int):
                        print("{}public Integer {};".format(self.tabs*"\t", prop))
                        continue
                    if isinstance(map[prop], list) and not self.isPrintingList:
                        print("{}/**".format(self.tabs*"\t"))
                        if len(map[prop]) > 0:
                            self.isPrintingList = True
                            self.tabs =+ 1
                            print_level(map[prop][0])
                            self.tabs =- 1
                            self.isPrintingList = False
                            print("{}*/".format(self.tabs*"\t"))
                        print("{}public List<> {};".format(self.tabs*"\t", prop))
                        continue
                    print("{}// print unknown: {} -> {}".format(self.tabs*"\t", prop, map[prop]))

            depth = 0
            print_level(map)

    def test_get_stations(self):
        stations = self.test_client.admin.public_api.get("/api/v1/configuration/stations/{}".format("eb83dda1-6a6b-49be-bc84-f0df2dfbfc52")).json()

        map = dict(stations)
        self.JavaJSONPrinter().print_java(map["properties"])


        # self.test_client.admin.public_api.get("/api/v1/configuration/stations")

    def test_get_languages(self):
        self.test_client.admin.public_api.get("/api/v1/languages")
        self.test_client.admin.config.get("/v1/organizations/{}/languages".format(self.test_client.admin.org))

    def test_get_sites(self):
        self.test_client.admin.public_api.get("/api/v1/configuration/sites")
        self.test_client.admin.config.get("/v1/organizations/{}/sites".format(self.test_client.admin.org))

    def test_create_sites(self):
        site_info = {
            "areaCodes": {
                "317": [
                    "555"
                ]
            },
            "countryCode": "1",
            "domain": "TestSite",
            "edges": [],
            "emergencyNumbers": [
                "911"
            ],
            "id": "4f4e6d0e-4446-4beb-9362-9cc0c5db9c03",
            "mainNumber": "3171234567",
            "name": "TestSite",
            "state": "ACTIVE",
            "tollFreeNumberPrefixes": [
                "800",
                "888",
                "817"
            ],
            "version": 2
        }
        self.test_client.admin.public_api.post("/api/v1/configuration/sites", json=site_info)

    def test_create_language(self):
        language_contract = {
            "state": "active",
            "label": "English - Written",
            "organizationId": "{}".format(self.test_client.admin.org),
            "version": 1
        }
        self.test_client.admin.config.post("/v1/organizations/{}/languages".format(self.test_client.admin.org), json=language_contract)

    def test_create_user_directory(self):
        user = {
            "email": "Test_User{}@example.com".format(uuid.uuid4())
        }
        self.test_client.admin.directory.post("/api/v1/users?autoCreateUser=true", json=user)

    def test_get_users_Directory(self):
        query = {
            "fl": "*",
            "includeDeleted": False,
            "offset": 0,
            "limit": 25
        }
        self.test_client.admin.directory.get("/api/v2/search/directory/people", params=query)

    def test_create_user_config(self):
        user = {
            "name": "testusercreate",
            "displayName": "testusercreate",
            "email":"Test_User{}@example.com".format(uuid.uuid4()),
            "username": "testusercreate@exmaple.com",
            "title":"test title",
            "department":"test department",
            "password":"test1234",
            "primaryContactInfo": [
                {
                    "address": "testusercreate@exmaple.com",
                    "type": "Primary",
                    "mediaType": "Email"
                }
            ]
        }
        self.test_client.admin.config.post("/v1/organizations/{}/users".format(self.test_client.admin.org), json=user)

    def test_get_edges(self):
        self.test_client.admin.public_api.get("/api/v1/configuration/edges")

    def test_get_roles(self):
        response = self.test_client.admin.public_api.get("/api/v1/authorization/roles")
        roles = response.json()

        print("{")

        print("}")

    def test_create_user(self):
        user_contract = {
            "name":"Test_User",
            "username":"Test_User",
            "email":"Test_User{}@example.com".format(uuid.uuid4()),
            # "displayName":"zcassels_test_user",
            "phoneNumber":"3175551234",
            # "roles": [{"id": roles_indexed["contentManagementUser"]["id"]}, {"id": roles_indexed["contentManagementAdmin"]["id"]}, {"id": roles_indexed["employee"]["id"]}],
            "department":"Department",
            "title":"MyTitle",
            "password":"test1234",
            "primaryContactInfo": [
                {
                    "address": "testusercreate@exmaple.com",
                    "type": "Primary",
                    "mediaType": "Email"
                }
            ]
            # "userImages":[]
        }

        response = self.test_client.admin.public_api.post("/api/v1/users/", json=user_contract)

    def test_query_orgs(self):
        self.test_client.admin.config.get("/v1/organizations/{}".format(self.test_client.admin.org))
        # self.test_client.admin.public_api.get("/api/v1/configuration/organizations/{}".format(self.test_client.admin.org))

    def test_add_features(self):
        organization_model = {
            "deletable": False,
            "modifiedDate": "2015-04-20T19:01:38.000+0000",
            "name": "CM_Testing_Org",
            "organizationFeatures": {
                "chat": True,
                "contactCenter": True,
                "custserv": False,
                "directory": True,
                "informalPhotos": False,
                "purecloud": True,
                "realtimeCIC": False,
                "unifiedCommunications": True
            },
            "state": "active",
            # "thirdPartyOrgName": "Admin_Prod_ANZ",
            "thirdPartyOrgId": "27",
            "version": 2,
            "voicemailEnabled": False
        }
        self.test_client.admin.config.put("/v1/organizations/{}".format(self.test_client.admin.org), json=organization_model)

    def test_create_org(self):
        organization_model = {
            "adminUsername": "ZC_TESTING_ORG@example.com",
            "adminPassword": "test1234",
            "name": "ZC_TESTING_ORG",
            "thirdPartyOrgName": "ZC_TESTING_ORG",
            "organizationFeatures": {
                "chat": True,
                "contactCenter": True,
                "custserv": True,
                "directory": True,
                "informalPhotos": False,
                "purecloud": True,
                "realtimeCIC": True,
                "unifiedCommunications": True
            },
        }
        self.test_client.admin.config.post("/v2/organizations", json=organization_model)

    def test_setup(self):
        self.testUser = self.test_client.get_session()

        # self.file = [('text_file', open('./Resources/CM_Text.txt', 'rb'), "text/plain")]
        self.file = [("text_file", ("CM_Text.text", open('./Resources/CM_Text.txt', 'rb'), "text/plain"))]

        # self.testUser.donut.get("/organizations/{}/users/{}".format(self.org, self.testUser.user_id))
        # self.testUser.donut.get("/organizations/{}/users/{}/roles".format(self.org, self.testUser.user_id))
        # self.testUser.donut.get("/organizations/{}/users/{}/permissions".format(self.org, self.testUser.user_id))

        timeout = time.time() + 60*5
        while time.time() < timeout:
            permissions_response = self.testUser.donut.get("/organizations/{}/users/{}/permissions".format(self.org, self.testUser.user_id))
            permissions = permissions_response.json()
            if len(permissions) == 4:
                logger.info("Polling for user to have permissions complete: {}".format(permissions))
                break
            time.sleep(.25)

        # Print user info
        workspace = {"type": "GROUP", "name": "zcassels_test_workspace_{}".format(uuid.uuid4())}
        create_ws_response = self.testUser.public_api.post("/api/v1/contentmanagement/workspaces/", json=workspace)
        self.ws = create_ws_response.json()

        def poll_ws():
            return self.testUser.public_api.get("/api/v1/contentmanagement/workspaces/" + self.ws["id"]).status_code == 200

        Retry.until_true(poll_ws, .25, 1)

        workspace_member = {"memberType": "USER"}
        self.testUser.public_api.put("/api/v1/contentmanagement/workspaces/{}/members/{}".format(self.ws["id"], self.testUser.user_id), json=workspace_member)

        logger.info("Test User -> {}".format(self.testUser.user_name))
        logger.info("Workspace -> {}".format(self.ws["name"]))