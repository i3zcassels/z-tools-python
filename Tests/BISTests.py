__author__ = 'zack.cassels'

import Tests.PureCloudTests, sys, requests

import logging, time
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

from Tests.PureCloudTests import PureCloudTests
from Common.HTTPResponseLogger import LogWrapper

class WebAPITests(PureCloudTests):
    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_bis_health(self):
        # self.test_client.admin.ad_hoc.get(full_url="http://qf-bulksimv3/BulkInteractionSimulator/health/check")
        # self.test_client.admin.bisv3.get("/health/check")
        base_cert_path = "/Users/zack.cassels/test-automation-framework/src/main/resources/com/inin/testing/cert/ServerCert.cer"
        base_cert_path = "/Users/zack.cassels/test-automation-framework/src/main/resources/com/inin/testing/cert/ClientCert.pfx"
        self.test_client.admin.ad_hoc.get(full_url="https://CloudAppsSims.inin-testing.com/BulkInteractionSimulator/health/check", cert=base_cert_path + "ServerCert.cer")

        # self.test_client.admin.ad_hoc.post(full_url="https://CloudAppsSims.inin-testing.com/BulkInteractionSimulator/SimulationRunners", verify=False)

        # bis_sim = {"identityTag": "tcAStationIsAbleToCallTheVoicemailExtension_svsOVQCN_4e327bd6eeb1",
        #     "shouldPersistIfServiceRestarts": False,
        #     "simulation": {
        #     "globalICServers": [{"iCServer":"QF-BulkSimv3"}],
        #     "schedules": [{
        #         "type": "PeriodicBurstSchedule",
        #         "maxInteractions":1,
        #         "startTimeMilliseconds": 0,
        #         "maxRunDurationMilliseconds":3600000,
        #         "maxOutstandingInteractions":0,
        #         "scenario": {
        #             "scenarioName":"checkVoicemail",
        #             "type":"OutboundCallScenario",
        #             "minimumAcceptableDurationMilliseconds":0,
        #             "maximumAcceptableDurationMilliseconds":922337203685477,
        #             "terminateInteractionsWhenAcceptableDurationExceeded":False,
        #             "timingLogPath":"",
        #             "disconnectOnActionFailure":True,
        #             "scoreInboundAudio":False,
        #             "sendDebugNotifications":True,
        #             "scoreIncomingAudioDeleteAcceptableRecordings":True,
        #             "interactionRecorderScoreDeleteAcceptableRecordings":True,
        #             "tos":["sip:*86@10.10.6.28:5060"],
        #             "sendAttributesAsAai":False,
        #             "froms":[{"callingPartyName":"AutoBISPhone_5_28_202gal_0","callingPartyNumber":None}],
        #             "actionCount":2,
        #             "atConclusion":"Disconnect",
        #             "waitForDisconnectTimeoutMilliseconds":600000,
        #             "actions":[{
        #                 "type":"SpotKeywordAction",
        #                 "comments":"",
        #                 "timeoutMilliseconds":30000,
        #                 "keywords":["new messages"],
        #                 "unexpectedKeywords":None,
        #                 "numSpots":-1,
        #                 "confidence":0.5,
        #                 "audioFileToQueueAndPlayWhileSpotting":None,
        #                 "waitForTimeout":False,
        #                 "inverse":False}
        #             ,{
        #                "type":"SpotKeywordAction",
        #                "comments":"",
        #                "timeoutMilliseconds":30000,
        #                "keywords":["then the choreographer must arbitrate"],
        #                "unexpectedKeywords":None,
        #                "numSpots":-1,
        #                "confidence":0.5,
        #                "audioFileToQueueAndPlayWhileSpotting":None,
        #                "waitForTimeout":False,
        #                "inverse":False
        #             }]
        #         }
        #         ,"rampUpTimeMilliseconds":0,
        #         "numInteractions":1,
        #         "burstIntervalMilliseconds":1000}
        #     ],
        #     "bISVersion":"3.1.0.0",
        #     "type":"Simulation",
        #     "stopOnInteractionFailure":True}
        # }
