__author__ = 'zack.cassels'

import Tests.PureCloudTests, sys

import logging, time
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

from Tests.PureCloudTests import PureCloudTests
from Common.HTTPResponseLogger import LogWrapper

class WebAPITests(PureCloudTests):
    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()


    def test_disassociate_users(self):

        users = ["fa6449ad-9c55-4bce-bf6b-65aa9b6ef494", "56c1e830-fd75-4db3-bc57-ea2e8f87f7c4"]

        for user in users:
            user_update = {"stationUri": ""}
            self.test_client.admin.public_api.put("/api/v1/users/{}".format(user), json=user_update)