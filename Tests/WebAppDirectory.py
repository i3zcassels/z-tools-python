__author__ = 'zack.cassels'

import Tests.PureCloudTests, sys

import logging, time
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

from Tests.PureCloudTests import PureCloudTests
from Common.HTTPResponseLogger import LogWrapper

class WebAPITests(PureCloudTests):
    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    class User():
        def __init__(self):
            self._id = None
            self.version = None

    def get_UI_user(self, name):
        results = self.test_client.admin.apps.get("/directory/api/v2/autocomplete?q={}".format(self.test_client.admin.user_name)).json()
        user = self.User()
        user._id = results["res"][0]["hits"][0]["_id"]
        user.version = results["res"][0]["hits"][0]["version"]
        logger.info("id -> '{}', version -> '{}'".format(user._id, user.version))
        return user

    def get_UI_admin(self):
        user = self.get_UI_user(self.test_client.admin.user_name)
        self.admin_id = user._id
        self.admin_version = user.version

    def test_user_settings(self):
        self.get_UI_admin()
        query={"fl": "*"}
        self.test_client.admin.apps.get("/directory/api/v2/people/{}".format("55673431d0e0981323fcc3f2"), params=query)

    def test_set_extension(self):
        self.get_UI_admin()
        #
        user_settings = {"value": {"number": "4321", "extension": "1234", "acceptsSMS": False}, "labelKey": "phone_work"}
        response = self.test_client.admin.apps.post("/directory/api/v2/people/{}/{}/field/contactInfo.phone_work".format(self.admin_id, self.admin_version), json=user_settings)
        phone_work = response.json()
        phone_id = phone_work["res"]["_id"]

        # self.test_client.admin.apps.delete("/directory/api/v2/people/{}/{}/field/contactInfo.phone_work/{}".format(self.admin_id, self.admin_version, "2fLTccOP8ZxPE9CcmMWIsf"))

    def test_get_session(self):

        # ININ-Session:90fa5ea2-3c51-4576-985f-be42f9ddf4ed

        # login = {
        #     "client":"web",
        #     "clientVersion":"2.7.0",
        #     "detail":
        #     {
        #         "userAgent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36",
        #         "semver":"2.7.0+1371-origin/release/2.7.0-d056ea7e0f7f10b5f54b95d2ec379af11952d168"
        #     },
        #     "includeFieldConfigs": True,
        #     "includeTranslations": True,
        #     "includeRoleInfo": True,
        #     "email":"zcassels_test_admin@example.com",
        #     "password":"test1234",
        #     "extendedSession": False,
        #     "lang":"en_us"
        # }
        # results = self.test_client.admin.apps.post("/api/v2/login", json=login).json()
        # session_id = results["res"]["X-OrgBook-Auth-Key"]
        # self.test_client.admin.apps.addAuthCookie(session_id)

        session = {}
        # results = self.test_client.admin.apps.post("/platform/api/v1/sessions", json=session).json()
        results = self.test_client.admin.public_api.post("/api/v1/sessions", json=session).json()

        self.test_client.admin.apps.headers.__setitem__("ININ-Session", results["id"])
        results = self.test_client.admin.apps.get("/platform/api/v1/users/{}".format(self.test_client.admin.user_id)).json()

    def test_get_user_id(self):
        user = self.test_client.admin.public_api.get("/api/v1/users/me").json()
        jabberId = user["chat"]["jabberId"]
        user_id = jabberId.split("@")[0]
        self.test_client.admin.apps.get("/directory/api/v2/people/{}".format(user_id))

    def test_query_users(self):
        self.test_client.admin.apps.get("/api/v1/users", params={})

    def test_query_user(self):
        query = {"includeInactive": True,
                 # "email": self.test_client.admin.user_name}
                 "email": "autoperson.hmgmfx1432239890639xasdf_xvcnrpwl_207ff6721e59@example.com"}
        # self.test_client.admin.apps.get("/directory/api/v1/users", params=query)
        self.test_client.admin.apps.get("/api/v1/users", params=query)

    def test_get_user(self):
        query={"fl": "*"}
        self.test_client.admin.apps.get("/directory/api/v1/people/{}".format(self.test_client.admin.user_id), params=query)

    def test_set_extension_throughUpdate(self):

        results = self.test_client.admin.apps.get("/directory/api/v2/autocomplete?q={}".format(self.test_client.admin.user_name)).json()
        user = results["res"][0]["hits"][0]

        user["contactInfo"]["phone_work"] = {
                "value": {
                    "acceptsSMS": False,
                    "extension": "1234",
                    "number": "4321"
                }
            }
        self.test_client.admin.apps.post("/directory/api/v2/people/{}".format(self.test_client.admin.user_id), json=user)