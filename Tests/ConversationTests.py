__author__ = 'zack.cassels'

import Tests.PureCloudTests, sys

import logging, time
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

from Tests.PureCloudTests import PureCloudTests
from Common.HTTPResponseLogger import LogWrapper

class WebAPITests(PureCloudTests):
    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_get_conversation(self):
        self.test_client.admin.public_api.get("/api/v1/conversations/{}".format("39b3b715-91fa-4ef6-928a-4df45ce27b4d"))