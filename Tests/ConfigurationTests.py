__author__ = 'zack.cassels'

import Tests.PureCloudTests, sys

import logging, time
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

from Tests.PureCloudTests import PureCloudTests
from Common.HTTPResponseLogger import LogWrapper

class EdgeTests(PureCloudTests):
    def setUp(self):
        super().setUp()
        # self.testUser = self.test_client.get_session()
        # self.ADMIN = "1bdf7e7f-977c-4dae-9c15-82567b2c8775"
        # self.USER = "42071eb6-4a85-4c3e-b8c3-ee2db55c1366"
        # self.USER = "42071eb6-4a85-4c3e-b8c3-ee2db55c1366"
        self.USER = "2f3b9aa4-5fd1-41f9-b34d-70ac9df1950b"

        self.SITE = "26fc9943-0d32-4431-852e-c02b07d7546c" # ZC
        # self.SITE = "f53c2a15-7f26-46a4-a56d-69ac06c79ecd" # DCA

        self.EXTENSION_PLAN = "8efe6390-f998-414f-9369-cfb034e8bed5"

        self.org = self.test_client.admin.org

    def tearDown(self):
        super().tearDown()


    def test_get_classifications(self):
        # self.test_client.admin.public_api.get("/api/v1/configuration/sites/")
        # self.test_client.admin.public_api.get("/api/v1/configuration/sites/{}/classifications".format(self.SITE))
        # self.test_client.admin.public_api.get("/api/v1/configuration/sites/{}/classifications/{}".format(self.SITE, self.EXTENSION_PLAN))

        dial_plan = {
            "createdBy": "unknown",
            "createdByApp": "unknown",
            "dateCreated": "2015-05-19T13:43:19.765Z",
            "id": "8efe6390-f998-414f-9369-cfb034e8bed5",
            "matchRegularExpression": "^(?:(\\d{4})|\\+15551231234;ext=(\\d{4}))$",
            "name": "Extension",
            "normalizedFormat": "$1$2",
            "priority": 2,
            "selfUri": "https://public-api.us-east-1.inindca.com/api/v1/configuration/sites/26fc9943-0d32-4431-852e-c02b07d7546c/classifications/8efe6390-f998-414f-9369-cfb034e8bed5",
            "state": "ACTIVE",
            "version": 1
        }
        # self.test_client.admin.public_api.put("/api/v1/configuration/sites/{}/classifications/{}".format(self.SITE, self.EXTENSION_PLAN), json=dial_plan)
        # self.test_client.admin.public_api.get("/api/v1/configuration/stations")
        self.test_client.admin.public_api.get("/api/v1/configuration/stations/{}".format("8dd745e3-70a0-4b4f-889f-3ec7349726e9"))

    def test_get_user_settings(self):
        # self.test_client.admin.public_api.get("/api/v1/configuration/extensions")
        # self.test_client.admin.public_api.get("/api/v1/configuration/extensionpools")
        # self.test_client.admin.public_api.get("/api/v1/users/me")
        user_update = {
            "phoneNumber": "+15556666"
        }
        # self.test_client.admin.public_api.put("/api/v1/users/{}".format(self.USER), json=user_update)


        # self.test_client.admin.public_api.get("/api/v1/users/{}".format(self.USER))
        # self.test_client.admin.public_api.get("/api/v1/users/")
        # self.test_client.admin.public_api.get("/api/v1/users", params={"username": "ZC_TESTING_ORG@example.com"})
        self.test_client.admin.public_api.get("/api/v1/users", params={"username": "AutoPerson.ddebhx1433175676729xtcAgentsOnDifferentSitesCanCallEachOtherExtensionNumber_DwDudnrg_c442ebadee5e"})
        # self.test_client.admin.config.get("/v1/organizations/{}/users/{}".format(self.test_client.admin.org, self.USER ))

        # self.test_client.admin.config.post("/v1/organizations/{}/users/".format(self.test_client.admin.org))
        # self.test_client.admin.config.get("/v1/organizations/{}/users/{}".format(self.test_client.admin.org, self.USER))

        # self.test_client.admin.config.get("/v1/organizations/{}/users/".format(self.test_client.admin.org))

        # password={"password": "password"}
        # self.test_client.admin.config.post("/v1/organizations/{}/users/{}/password".format(self.test_client.admin.org, self.ADMIN), json=password)

    def test_get_org_info(self):

        # self.test_client.admin.public_api.get("/api/v1/configuration/voicemailpolicy")

        # voice_mail = {
        #     "alertTimeoutSeconds": 15,
        #     "compressSilence": True,
        #     "createdDate": "2015-05-18T18:20:17.366Z",
        #     "enabled": True,
        #     "fullMessageUri": "https://dev-voicemail-service.s3.amazonaws.com/organizations/a829dde2-c800-4942-9154-e5e21a19ecc1/config/full.wav",
        #     "maximumRecordingTimeSeconds": 120,
        #     "minimumRecordingTimeSeconds": 2,
        #     "modifiedDate": "2015-05-21T13:48:20.551Z",
        #     "namePromptMessageUri": "https://dev-voicemail-service.s3.amazonaws.com/organizations/a829dde2-c800-4942-9154-e5e21a19ecc1/config/nameprompt.wav",
        #     "pinConfiguration": {
        #     "maximumLength": 4,
        #     "minimumLength": 4
        #     },
        #     "quotaSizeBytes": 20971520,
        #     "retentionTimeDays": 190,
        #     "unavailableMessageUri": "https://dev-voicemail-service.s3.amazonaws.com/organizations/a829dde2-c800-4942-9154-e5e21a19ecc1/config/unavailable.wav",
        #     "voicemailExtension": "*86"
        # }
        # # voice_mail = {
        # #     "enabled": True,
        # # }
        # self.test_client.admin.public_api.put("/api/v1/configuration/voicemailpolicy", json=voice_mail)
        self.test_client.admin.voice_mail.get("/v1/organizations/{}/config".format(self.org))

    def test_get_org_info_voice_mail(self):
        # voice_mail =  {
        #   "alertTimeout": 12,
        #   "compressSilence": True,
        #   "createdDate": "2015-05-18T18:20:17.366+0000",
        #   "enabled": True,
        #   "fullMessageURL": "https://dev-voicemail-service.s3.amazonaws.com/organizations/a829dde2-c800-4942-9154-e5e21a19ecc1/config/full.wav",
        #   "maximumRecordingTime": 120,
        #   "minimumRecordingTime": 2,
        #   "modifiedDate": "2015-05-21T13:43:58.050+0000",
        #   "namePromptMessageURL": "https://dev-voicemail-service.s3.amazonaws.com/organizations/a829dde2-c800-4942-9154-e5e21a19ecc1/config/nameprompt.wav",
        #   "organizationId": "a829dde2-c800-4942-9154-e5e21a19ecc1",
        #   "pinConfiguration": {
        #     "algorithm": "PBKDF2",
        #     "iterations": 2000,
        #     "keyLength": 10,
        #     "maxLength": 4,
        #     "minLength": 4,
        #     "saltLength": 10
        #   },
        #   "quotaSize": 20971520,
        #   "retentionTime": 190,
        #   "unavailableMessageURL": "https://dev-voicemail-service.s3.amazonaws.com/organizations/a829dde2-c800-4942-9154-e5e21a19ecc1/config/unavailable.wav"
        # }
        self.test_client.admin.voice_mail.put("/v1/organizations/{}/config".format(self.org), json=voice_mail)


    def test_clean_extension_pool(self):
        ext_pools = self.test_client.admin.public_api.get("/api/v1/configuration/extensionpools").json()
        for ext_pool in ext_pools["entities"]:
            self.test_client.admin.public_api.delete("/api/v1/configuration/extensionpools/{}".format(ext_pool["id"]))


    def test_create_extension_pool(self):
        # self.test_client.admin.public_api.get("/api/v1/configuration/extensionpools")

        extension_pool = {
            "endNumber": "222",
            "id": "1620fe98-2f5a-488c-95c5-af194c576786",
            "selfUri": "https://public-api.us-east-1.inindca.com/api/v1/configuration/extensionpools/1620fe98-2f5a-488c-95c5-af194c576786",
            "startNumber": "222"
        }
        self.test_client.admin.public_api.get("/api/v1/configuration/extensions")


    def test_start_update(self):
        self.test_client.admin.public_api.post("/api/v1/")

    def test_get_stations(self):

        self.test_client.admin.public_api.get("/api/v1/stations")

        # station_id = "e1f2cd5f-4262-43bd-9a61-30699bd541e0"

        # station_id = "e9b43f50-9234-432d-80f5-5d8c4788fa46"

        # current station?
        # station_id = "1607d038-b45c-4dcd-968b-bac9330b9f22"
        phone = "9078c1c6-2552-463e-b5b0-a514d8980700"

        # user_update = {"stationUri": "https://apps.inindca.com/platform/api/v1/stations/{}".format(station_id)}
        # self.test_client.admin.public_api.put("/api/v1/users/{}".format(self.test_client.admin.user_id), json=user_update)
        # self.test_client.admin.config.get("/v1/organizations/{}/stations/{}".format(self.test_client.admin.org, station_id))
        # self.test_client.admin.config.get("/v1/organizations/{}/phones/{}".format(self.test_client.admin.org, phone))