__author__ = 'zack.cassels'

import Tests.PureCloudTests, sys

import logging, time
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

from Tests.PureCloudTests import PureCloudTests
from Common.HTTPResponseLogger import LogWrapper

class WebAPITests(PureCloudTests):
    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_get_org(self):
        self.test_client.admin.directory.get("/api/v1/org")

    def test_enable_voicemail(self):
        update = {
            "version": 5,
            "purecloud": {
                "voicemailEnabled": [
                    {
                        "_id": "3EwgEjKni14NDL6EpNXTIV",
                        "labelKey": "voicemailEnabled",
                        "value": True
                    }
                ]
            },
        }
        self.test_client.admin.directory.post("/api/v1/org", json=update)