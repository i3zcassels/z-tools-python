__author__ = 'zack.cassels'

import Tests.PureCloudTests, sys

import logging, time
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

from Tests.PureCloudTests import PureCloudTests
from Common.HTTPResponseLogger import LogWrapper

class EdgeTests(PureCloudTests):
    def setUp(self):
        super().setUp()
        self.testUser = self.test_client.get_session()
        # self.EDGE_ID = "99b34620-75be-4f2e-97eb-f057abf9075b"
        # self.EDGE_ID = "07c3db2f-48bf-4189-8382-63328f24eba4"
        self.EDGE_ID = "57f6c60d-6f62-47b2-a587-80b51e823e53"
        self.LINE_TEMPLATE = "eb9bfa2f-7e50-4d3e-ad3d-a1a8f699c423"

    def tearDown(self):
        super().tearDown()

    def test_get_edges(self):
        query={"name": "AutoTestEdgeA"}
        response = self.test_client.admin.public_api.get("/api/v1/configuration/edges", params=query)
        id = response.json()["entries"][0]["id"]
        self.test_client.admin.edge_config.get("/v1/edges/{}/tenants/{}/dialplan/all".format(id, self.test_client.admin.org))

    def test_get_edges_softwareVersion(self):
        # self.test_client.admin.public_api.get("/api/v1/configuration/edges/")
        # self.test_client.admin.public_api.get("/api/v1/configuration/edges/{}".format(self.EDGE_ID))
        # self.test_client.admin.public_api.get("/api/v1/configuration/edges/{}/softwareversions".format(self.EDGE_ID))
        # http://public-api.us-east-1.inindca.com:80/api/v1/configuration/edges/07c3db2f-48bf-4189-8382-63328f24eba4/softwareupdate
        # software_update = {"id":"07c3db2f-48bf-4189-8382-63328f24eba4",
        #  "status":"FAILED",
        #  "version": {
        #      "id": "1.0.0.2314",
        #      "edgeVersion": "1.0.0.2314",
        #      "latestRelease": False,
        #      "current": False,
        #      "edgeUri": "api/v1/configuration/edges/07c3db2f-48bf-4189-8382-63328f24eba4"
        #  },
        #  "executeStopTime": "2015-05-22T20:19:29+0000",
        #  "isCanceling": False}
        # self.test_client.admin.public_api.post("/api/v1/configuration/edges/{}/softwareupdate".format(self.EDGE_ID), json=software_update)
        # self.test_client.admin.public_api.get("/api/v1/configuration/edges/{}/logicalinterfaces".format(self.EDGE_ID))
        # self.test_client.admin.public_api.get("/api/v1/configuration/edges/{}/physicalinterfaces".format(self.EDGE_ID))
        self.test_client.admin.public_api.get("/api/v1/configuration/edges/{}/lines".format(self.EDGE_ID))
        # self.test_client.admin.public_api.get("/api/v1/configuration/edges/certificateauthorities")


    def test_get_templates(self):
        # http://configuration.us-east-1.inindca.com/configurations/v1/organizations/4a251891-0232-465c-8694-c5928a4646ef/phoneTemplates/eb9bfa2f-7e50-4d3e-ad3d-a1a8f699c423
        # self.test_client.admin.public_api.get("/api/v1/configuration/phonetemplates")
        self.test_client.admin.public_api.get("/api/v1/configuration/linetemplates")
        # self.test_client.admin.public_api.get("/api/v1/configuration/lines")
        # self.test_client.admin.config.get("/v2/organizations/{}/lineTemplates".format(self.testUser.org))
        # self.test_client.admin.config.get("/v1/organizations/{}/phoneTemplates/".format(self.testUser.org))
        # self.test_client.admin.config.get("/v1/organizations/{}/lineTemplates/".format(self.testUser.org))
        # self.test_client.admin.config.get("/v1/organizations/{}/lineTemplates/".format(self.testUser.org))

    def test_get_dial_plan(self):
        # self.test_client.admin.edge_config.get("/v1/edges/{}/tenants/{}/dialplan/all".format(self.EDGE_ID, self.test_client.admin.org))

        query={"name": "AutoTestEdgeA"}
        response = self.test_client.admin.public_api.get("/api/v1/configuration/edges", params=query)
        id = response.json()["entities"][0]["id"]
        self.test_client.admin.edge_config.get("/v1/edges/{}/tenants/{}/dialplan/all".format(id, self.test_client.admin.org))

    def test_get_homed_stations(self):
        query={"name": "AutoTestEdgeA"}
        response = self.test_client.admin.public_api.get("/api/v1/configuration/edges", params=query)
        id = response.json()["entities"][0]["id"]

        # self.test_client.admin.edge_config.get("/v1/organizations/{}/edges/{}/stations".format(self.test_client.admin.org, id, "8dd745e3-70a0-4b4f-889f-3ec7349726e9"))
        # self.test_client.admin.edge_config.get("/v1/edges/{}/tenants/{}/stations".format(id, self.test_client.admin.org))
        # self.test_client.admin.edge_config.get("/v1/edges/{}/tenants/{}/users/info".format(id, self.test_client.admin.org))
        self.test_client.admin.edge_config.get("/v1/edges/{}/tenants/{}/users/config".format(id, self.test_client.admin.org))
        self.test_client.admin.edge_config.get(full_url="http://edge-config.us-east-1.inindca.com/edge-config/v1/organizations/4a251891-0232-465c-8694-c5928a4646ef/edgegroups/35372b7c-671f-4b9f-b118-54db6e4ef7b3/users/config/981e7b89-b537-4f56-a598-d23feadc1cba")
        # self.test_client.admin.edge_config.get("/v1/edges/{}/tenants/{}/users/config/{}".format(id, self.test_client.admin.org, "981e7b89-b537-4f56-a598-d23feadc1cba"))

    def test_print_users(self):
        query={"name": "AutoTestEdgeA"}
        response = self.test_client.admin.public_api.get("/api/v1/configuration/edges", params=query)
        id = response.json()["entities"][0]["id"]

        response = self.test_client.admin.edge_config.get("/v1/edges/{}/tenants/{}/users/config".format(id, self.test_client.admin.org))
        user_list = response.json()
        users = []
        for user_url in user_list:
            request = self.test_client.admin.edge_config.get(full_url=user_url)
            if request.status_code == 200:
                users.append(request.json())

        for user in users:
            logger.info("user -> '{}' ('{}')".format(user["user.username"], user["user.id"]))
            for alias in user["user.aliases"]:
                logger.info("    |- alias -> '{}'".format(alias))


    def test_print_user(self):
        "/v1/organizations/4a251891-0232-465c-8694-c5928a4646ef/edgegroups/35372b7c-671f-4b9f-b118-54db6e4ef7b3/users/config/32fef88a-3882-4b15-9ace-f4baccfc31af"
        # response = self.test_client.admin.public_api.get("/api/v1/users", params={"username": "AutoPerson.ddebhx1433175676729xtcAgentsOnDifferentSitesCanCallEachOtherExtensionNumber_DwDudnrg_c442ebadee5e"})
        response = self.test_client.admin.public_api.get("/api/v1/users", params={"username": "zctestingperson@example.com"})
        users_query = response.json()
        user_id = users_query["entities"][0]["id"]
        self.test_client.admin.edge_config.get("/v1/organizations/{org}/edgegroups/{edge_group}/users/info/{user}".format_map({
            "org": self.test_client.admin.org,
            "edge_group": "35372b7c-671f-4b9f-b118-54db6e4ef7b3",
            "user": user_id}))
        self.test_client.admin.edge_config.get("/v1/organizations/{org}/edgegroups/{edge_group}/users/config/{user}".format_map({
            "org": self.test_client.admin.org,
            "edge_group": "35372b7c-671f-4b9f-b118-54db6e4ef7b3",
            "user": user_id}))


    def test_print_stations(self):
        query={"name": "AutoTestEdgeA"}
        response = self.test_client.admin.public_api.get("/api/v1/configuration/edges", params=query)
        id = response.json()["entities"][0]["id"]

        response = self.test_client.admin.edge_config.get("/v1/edges/{}/tenants/{}/stations".format(id, self.test_client.admin.org))
        stationList = response.json()
        stationNames = []
        for station in stationList:
            request = self.test_client.admin.edge_config.get(full_url=station)
            if request.status_code == 200:
                stationNames.append(request.json()["station.name"])

        for stationName in stationNames:
            logger.info("station -> '{}'".format(stationName))

    def test_print_station(self):

        # /v1/organizations/4a251891-0232-465c-8694-c5928a4646ef/edges/07c3db2f-48bf-4189-8382-63328f24eba4/stations/e6f36246-af96-42ca-9ba4-ad579eaaf286

        response = self.test_client.admin.public_api.get("/api/v1/stations", params={"name": "ZcBISPhone1"})
        users_query = response.json()
        station_id = users_query["entities"][0]["id"]

        self.test_client.admin.edge_config.get("/v1/organizations/{org}/edges/{edge}/stations/{station}".format_map({
            "edge": "07c3db2f-48bf-4189-8382-63328f24eba4",
            "org": "4a251891-0232-465c-8694-c5928a4646ef",
            "station": station_id}))