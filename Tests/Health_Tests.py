__author__ = 'zack.cassels'

import Tests.PureCloudTests, sys

import logging, time, unittest
logging.getLogger("requests").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

from Tests.PureCloudTests import PureCloudTests
from Common.HTTPResponseLogger import LogWrapper


class ESTests(unittest.TestCase):

    def test_ES_audit(self):
        # requests.get("http://es-audit-service.us-east-1.inintca.com:9200/_cluster/health?pretty=true", hooks=dict(response=verboseLog))
        # requests.get("http://es-contentmanagement.us-east-1.inintca.com:9200/_cluster/health?pretty=true", hooks=dict(response=verboseLog))
        pass

class ServiceHealth(PureCloudTests):
    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_health_checks(self):
        # health_info = self.test_client.admin.public_api.get("/health/check")
        # self.test_client.admin.donut.get("/health/check")
        # self.test_client.admin.donut_events.get("/health/check")
        # self.test_client.admin.directory.get("/api/v1/status")
        # self.test_client.admin.audit.get("/health/check")
        self.test_client.admin.config.get("/health/check")
        # self.test_client.admin.cm.get("/health/check")