__author__ = 'zack.cassels'

import unittest, logging.config, logging, uuid, yaml, time, requests, datetime
logging.config.fileConfig('logging.ini')
logging.getLogger("requests").setLevel(logging.WARNING)
from Common.HTTPResponseLogger import verboseLog, noLog
from TestClient import Test_Client, Environment
logger = logging.getLogger(__name__)

class PureCloudTests(unittest.TestCase):
    def setUp(self):
        env = Environment.DEVELOPMENT
        self.test_client = Test_Client(env, False)

    def tearDown(self):
        self.test_client.close()

    def test_get_edges_status(self):

        edges = ["AutoTestEdgeA",
                 "AutoTestEdgeB",
                 "AutoTestEdgeC",
                 "AutoTestEdgeD",
                 "AutoTestEdgeE",
                 "AutoTestEdgeF"]

        for edge in edges:
            query = {"Hostname": edge}
            EMO = self.test_client.admin.edge_helper.get("/api/EdgeManufacturing", params=query, hooks=dict(response=noLog)).json()

            time = ''
            if EMO["underTest"] != "False":
                isUnderTest = EMO["underTest"].split(" ")[0]
                timestamp = int(EMO["underTest"].split(" ")[1])

                time = datetime.datetime.fromtimestamp(timestamp / 1e3)
                print("'{}' ({}): underTest -> '{}', timestamp -> '{}'".format(edge, EMO["ipAddress"], isUnderTest, time))
            else:
                print("'{}' ({}): underTest -> '{}'".format(edge, EMO["ipAddress"], EMO["underTest"], time))
